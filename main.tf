module "vpc_1" {
  source = "terraform-aws-modules/vpc/aws"

  azs = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}
