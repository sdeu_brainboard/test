variable "tags" {
  description = "Default tags to apply to all resources."
  type        = map(any)
}

variable "custom" {
  description = "testing"
  type = string
  default = "awesome"
}